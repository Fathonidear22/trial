// Dengan menggunakan tempalte literal kita dapat menggunakan teks dan variable dengan tampilan yang lebih rapi.

let word1 = 'Air'
word1 = 'teh'
// const word2 ='Air'
let word2 ='teh'
word2 = 'madu'

// word1 = 'Air berubah'
// console.log(word2)..

let gabungan = 'kalimat ' + word1 + ' ' + word2 + 'ini adalah gabungan dari word1 dan word2'
// let gabungan = `kalimat ${word1} ${word2}  adalah gabungan dari word1 dan word2`
console.log(gabungan)

//let num1 =5
// let num2 =12
// console.log (`doni mempunyai ${num1 + num2 } gelas $(word1) $(word2)`)