// import { resolve } from "dns";
// import { rejects } from "assert";

//membongkar nilai dari object atau array kedalam variable terpisah
// let hewan = [`ikan`, `kucing`, `ayam`, 4]
// shift
// let x = hewan.shift() //index pertama
// pop
// let x = hewan.push (`test`) //index terakhir
//push
// let x = hewan.push(`test`) //menambah array
// splice
//  let z = hewan.splice(3,1) //menghapus atau mengganti array
//  let z = hewan.splice(0,1, 'test')
 
//  console.log(hewan)

// const personalInformation = {
//     firstName: 'Fathoni',
//     lastName: 'Rochman',
//     status: 'Mahasiswa',
//     state: 'Bogor',
//     zipCode: 16310
// }

// const {firstName, lastName, status, state, zipCode} = personalInformation
// // console.log(firstName)
// personalInformation.state = 'Bogor'

// console.log(personalInformation)
// cara lama
//const hewan = [8, `anjing`, `daging`, `berbulu`]
// console.log(jmlkaki)

//const [jmlkaki, namahewan, makanan, ciri] = hewan
//console.log(jmlkaki)

// rest parameter 
// let [a, b, c,] = [1, 2, 3]
// console.log(a)

// let [a, b, c, ...rtyas] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
// console.log(rtyas[1])


// looping arrays
// let hewan = [`kelinci`,`kucing`,`anjing`,`kuda`,`merpati`]
// console.log(hewan.length)
// for (let i=0, len=hewan; i<len; i++){
    // console.log(hewan[i])

// let arr = [   
    // {
        // firstName: "Dani",
        // lastName: "test",
        // nilai: 99
    // },

    // {
        // firstName: "Agus",
        // lastName: "test",
        // nilai: 88
    // },
// ]

// hasil = arr.sort()
// console.log(hasil)

// function detikSatu() {
//     setTimeout(() => {
//       console.log('detik 1')
//     }, 9500);
// }

// function detikDua() {
//     setTimeout(() => {
//         console.log('detik 2')
//     }, 800);
// }

// detikSatu()
// detikDua()


// function tambahSiswa(siswa, callback)
// {
    //     kelas3.push(siswa);
    //     console.log('Selesai push');
    //     callback();
    // }
    
    // function getKelas()
    // {
        //     console.log('Kelas3',kelas3);
        // }
        
        // getKelas()
// tambahSiswa({nis:3,nama:'Benyamin'},getKelas);

let kelas3 = [
    {nis:1, nama:'Winnerdy'},
    {nis:2, nama:'Setiabudi'},
]

function tambahSiswa(siswa)
{
    return new Promise((resolve, reject) => {
        try{ 
            resolve ()
            kelas3.push(siswa);
        }catch(err){
            console.log(err)
        }
    
       

    })

}

function getKelas()

{
    console.log('kelas3 dalam getClass (baris 36):' ,kelas3);
}

async function init() {
    await tambahSiswa ({nis:3, nama:'Rinjani'})
    await getKelas()
}

init()

